﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xCore
{
    class Program
    {
        static void Main(string[] args)
        {
            MinecraftServer server = new MinecraftServer();
            server.Start();
            server.AddCommand(new StopCommand());
            server.AddCommand(new InfoCommand());
            server.AddCommand(new HelpCommand());
            while (server.Enabled)
            {
                // Ох, ня. Система комманд работает с первого (!) раза о_О
                /*
                 * Чтобы добавить новую команду, нужно:
                 * 1) Создать новый класс-наследник интерфейса Command
                 * 2) Добавить туда методы Call(Object sender, MinecraftServer server); GetName(), который возвратит название команды
                 * 3) Зарегистрировать её методом MinecraftServer.AddCommand(Command yourCommand) */
                string readString = Console.ReadLine();
                Command cmd = server.GetCommand(readString);
                if (cmd == null)
                {
                    Logger.Log("Такая комманда не найдена!");
                    continue;
                }
                cmd.Call(null, server);
            }
        }
    }
}
