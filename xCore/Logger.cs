﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xCore
{
    class Logger
    {
        public static void Log(string log)
        {
            DateTime now = DateTime.Now;
            Console.WriteLine("[{0}.{1}.{2} {3}:{4}:{5}] {6}", now.Day, now.Month, now.Year, now.Hour, now.Minute, now.Second, log);
        }
    }
}
