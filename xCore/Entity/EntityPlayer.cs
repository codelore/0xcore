﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace xCore
{
    class EntityPlayer
    {
        public MinecraftServer server;

        public String displayName;

        public int playerPing;

        private Socket socket;

        public EntityPlayer(Socket socket)
        {
            this.socket = socket;
        }

        public void Send(byte[] buffer)
        {
            socket.Send(buffer);
        }

        public void Kick()
        {
            socket.Disconnect(true);
        }

        public String getPlayerName()
        {
            return this.displayName;
        }

        public int getPing()
        {
            return this.playerPing; 
        }
    }
}
