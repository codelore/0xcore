﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace xCore
{
    class MinecraftServer
    {
        public Socket ServerSocket;
        /** */
        private NetworkServer NetServer;
        /** Основной поток тиков */
        private Thread ServerThread;
        /** Если сервер включен - true, иначе - false. Служит для механизма тиков */
        public bool Enabled;
        /** Команды */
        private Hashtable Commands;
        /** Текущий тик */
        private int tick;
        /** Свойство, инкапсулирующее тик */
        public int Tick
        {
            get
            {
                return tick;
            }
            set
            {
                if (value - 1 == Tick)
                {
                    tick = value;
                }
            }
        }
        /** Конструктор класса */
        public MinecraftServer()
        {
            try
            {
                Enabled = true;
                Commands = new Hashtable();
                NetServer = new NetworkServer("127.0.0.1",25565);
                ServerThread = new Thread(TickServer);
                ServerThread.IsBackground = true;
            }
            catch (Exception e) 
            {
                Logger.Log("Ошибка: " + e.StackTrace);
            }
        }

        /** Запуск потоков сервера и сети */
        public void Start()
        {
                // TODO: Сделать сетевой поток
                ServerThread.Start();
                try
                {
                    NetServer.Start();
                    Logger.Log("Сетевой сервер запущен на "+ NetServer.getIpAddress() + ":" + NetServer.getPort()+".");
                }
                catch (Exception e)
                {
                    Logger.Log("Ошибка! - " + e + "\n" + "Stack Trace: " + e.StackTrace);
                }
                Logger.Log("Сервер запущен!");
            }

        /** Остановка сервера */
        public void Stop()
        {
            // Сохранение и остановка всех игровых объектов
            Enabled = false;
            Logger.Log("Сервер выключен!");
            Thread.Sleep(500);
        }

        /** Игровые тики */
        private void TickServer()
        {
            // TODO: Сделать простейшую систему тиков
            // TODO: Сделать расчет времени до следующего тика, относительно законичившегося для того, чтобы сервер обрабатывал нужное количество тиков в секунду
            while (Enabled)
            {
                Tick++;
                Thread.Sleep(1000); // Это просто так
            }
            ServerThread.Abort();
        }

        public void AddCommand(Command command)
        {
            Commands.Add(command.GetName(), command);
        }

        public Command GetCommand(String command)
        {
            if(Commands.ContainsKey(command))
            {
                foreach(Command cmd in Commands.Values)
                {
                    if(cmd.GetName().ToLower() == command.ToLower())
                    {
                        return cmd;
                    }
                }
            }
            return null;
        }
    }
}
