﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xCore
{
    interface Command
    {
        void Call(Object sender, MinecraftServer server);
        string GetName();
    }
}
