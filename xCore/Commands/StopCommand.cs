﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xCore
{
    class StopCommand : Command
    {
        public void Call(Object sender, MinecraftServer server)
        {
            server.Stop();
        }

        public string GetName()
        {
            return "stop";
        }
    }
}
