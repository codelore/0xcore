using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xCore
{
    class HelpCommand : Command
    {
        public void Call(Object sender, MinecraftServer server)
        {
            if (sender == null)
            {
                Logger.Log("Info, Stop, Help");
            }
        }

        public string GetName()
        {
            return "help";
        }
    }
}